option('backgrounds', type: 'boolean', value: false, description:'zrya wallpapers')
option('plymouth', type: 'boolean', value: false, description:'build plymouth-theme component')

option('gnome-shell', type: 'boolean', value: true, description:'build gnome-shell component')
option('gnome-shell-gresource', type: 'boolean', value: true, description: 'build gnome-shell component in gresources')

option('gtk', type: 'boolean', value: true, description:'build gtk component')
option('default', type: 'boolean', value: true, description:'build gtk default flavour')
option('dark', type: 'boolean', value: true, description:'build gtk dark flavour')

option('sessions', type: 'boolean', value: true, description:'build sessions component')
